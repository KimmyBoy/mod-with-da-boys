package com.SuperD4rkChoopertat.hfdwthyw.tools;

import com.SuperD4rkChoopertat.hfdwthyw.util.RegistryHandler;
import net.minecraft.item.IItemTier;
import net.minecraft.item.crafting.Ingredient;

import java.util.function.Supplier;

public enum HfdwthywItemTier implements IItemTier {


    //1+ baseDamage + addedDamage baseDamage here, addedDamage in registryHandler, 1 from vanilla mc
    //these are item tiers, not specific items themselves, please dont mess it up
    /*
    ill leave this here for making new item tiers later(convenience) just ctrl+c ctrl+v

    TIER_NAME(0, 0, 0F, 0F, 0, () -> {
        return Ingredient.fromItems(RegistryHandler.NAME_OF_REPAIR_MATERIAL.get());
    });
    also the ; above this line should be turned into a comma for new tiers
     */
    AWAKENED_FINGER_SWORD(0, 342, 1.0F, 8.0F, 7, () -> {
        return Ingredient.fromItems(RegistryHandler.FINGER_SWORD.get());
    }),

    URANIUM_ICE(3, 1783, 12.0F, 7.0F, 23, () -> {
        return Ingredient.fromItems(RegistryHandler.ICE_CHUNK.get());
    });

    private final int harvestLevel;
    private final int maxUses;
    private final float efficiency;
    private final float attackDamage;
    private final int enchantability;
    private final Supplier<Ingredient> repairMaterial;

    HfdwthywItemTier(int harvestLevel, int maxUses, float efficiency, float attackDamage, int enchantability, Supplier<Ingredient> repairMaterial) {

        this.harvestLevel = harvestLevel;
        this.maxUses = maxUses;
        this.efficiency = efficiency;
        this.attackDamage = attackDamage;
        this.enchantability = enchantability;
        this.repairMaterial = repairMaterial;

    }

    @Override
    public int getMaxUses() {
        return maxUses;
    }

    @Override
    public float getEfficiency() {
        return efficiency;
    }

    @Override
    public float getAttackDamage() {
        return attackDamage;
    }

    @Override
    public int getHarvestLevel() {
        return harvestLevel;
    }

    @Override
    public int getEnchantability() {
        return enchantability;
    }

    @Override
    public Ingredient getRepairMaterial() {
        return repairMaterial.get();
    }
}
