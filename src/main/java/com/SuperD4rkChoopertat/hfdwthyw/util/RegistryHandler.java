package com.SuperD4rkChoopertat.hfdwthyw.util;

import com.SuperD4rkChoopertat.hfdwthyw.HFDWTHYW;
import com.SuperD4rkChoopertat.hfdwthyw.blocks.BlockItemBase;
import com.SuperD4rkChoopertat.hfdwthyw.blocks.DritBlock;
import com.SuperD4rkChoopertat.hfdwthyw.items.ItemBase;
import com.SuperD4rkChoopertat.hfdwthyw.tools.HfdwthywItemTier;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.PickaxeItem;
import net.minecraft.item.SwordItem;
import net.minecraftforge.common.Tags;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class RegistryHandler {

   public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, HFDWTHYW.MOD_ID);
   public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, HFDWTHYW.MOD_ID);

   public static void init() {
      ITEMS.register(FMLJavaModLoadingContext.get().getModEventBus());
      BLOCKS.register(FMLJavaModLoadingContext.get().getModEventBus());
   }

   //Items
   //add comma to add entry, make sure last entry has no comma(en_us.json)
   public static final RegistryObject<Item> FINGER_SWORD = ITEMS.register( "finger_sword",  ItemBase::new);

   public static final RegistryObject<Item> ICE_CHUNK = ITEMS.register( "ice_chunk",  ItemBase::new);

   //Tools
   /* attackSpeed is strange. 4 is default and is like pre 1.9 combat.
    you must minus from 4(so like -2.4F for reg swords(0.6 atk speed)) the higher the number the quicker it is
   */
   public static final RegistryObject<SwordItem> URANIUM_ICE_LONGSWORD = ITEMS.register("uranium_longsword", () ->
           new SwordItem(HfdwthywItemTier.URANIUM_ICE, 4, -2.6F, new Item.Properties().group(ItemGroup.COMBAT)));

   public static final RegistryObject<SwordItem> AWAKENED_FINGER_SWORD = ITEMS.register("awakened_finger_sword", () ->
           new SwordItem(HfdwthywItemTier.AWAKENED_FINGER_SWORD, 1, -2.2F, new Item.Properties().group(ItemGroup.COMBAT)));

   public static final RegistryObject<PickaxeItem> URANIUM_ICEPICK = ITEMS.register("uranium_pick", () ->
           new PickaxeItem(HfdwthywItemTier.URANIUM_ICE, -4, -1.0F, new Item.Properties().group(ItemGroup.TOOLS)));



   //Blocks
   public static final RegistryObject<Block> DRIT_BLOCK = BLOCKS.register("drit_block", DritBlock::new);

   //Block items
   public static final RegistryObject<Item> DRIT_BLOCK_ITEM = ITEMS.register("drit_block", () -> new BlockItemBase(DRIT_BLOCK.get()));

}

